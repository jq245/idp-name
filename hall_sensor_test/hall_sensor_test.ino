const int HALL_SENSOR_PIN = 3;


void setup() {
  // put your setup code here, to run once:
  pinMode(HALL_SENSOR_PIN, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (digitalRead(HALL_SENSOR_PIN) == HIGH){
    Serial.println("Active block detected");
  }

  else if (digitalRead(HALL_SENSOR_PIN) == LOW){
    Serial.println("Inactive block detected");
  }

  delay(100);
}
