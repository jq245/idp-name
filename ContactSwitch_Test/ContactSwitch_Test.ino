const int FrontContactSwitchPin = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(FrontContactSwitchPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (digitalRead(FrontContactSwitchPin) == HIGH)
  {
    Serial.println("Front switch ON");
    
  }
  else if (digitalRead(FrontContactSwitchPin) == LOW)
  {
    Serial.println("Front switch OFF");
    
  }
}
