
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
void move_wheels(int v_l, int v_r){
  
    Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
    Adafruit_DCMotor *myMotor_l = AFMS.getMotor(1);
    Adafruit_DCMotor *myMotor_r = AFMS.getMotor(2);
    // create with the default frequency 1.6KHz
    AFMS.begin(); 

  //check if both input speed are positive
  if(v_l >=0 and v_r >=0){
    myMotor_l->setSpeed(v_l);myMotor_r->setSpeed(v_r);
    myMotor_l->run(FORWARD);myMotor_r->run(FORWARD);
    }

  else if(v_l <0 and v_r >=0){
    myMotor_l->setSpeed(-v_l); myMotor_r->setSpeed(v_r);
    myMotor_l->run(BACKWARD);myMotor_r->run(FORWARD);
    }
  else if(v_l >=0 and v_r <0){
    myMotor_l->setSpeed(v_l); myMotor_r->setSpeed(-v_r);
    myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
    }

   else if(v_l <0 and v_r <0){
    myMotor_l->setSpeed(-v_l); myMotor_r->setSpeed(-v_r);
    myMotor_l->run(BACKWARD);myMotor_r->run(BACKWARD);
    }
    
    
    
    
  }

void turn_90_right(){
    Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
    Adafruit_DCMotor *myMotor_l = AFMS.getMotor(1);
    Adafruit_DCMotor *myMotor_r = AFMS.getMotor(2);
    // create with the default frequency 1.6KHz
    AFMS.begin(); 
    myMotor_l->setSpeed(100); myMotor_r->setSpeed(0);
    myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
    delay(2500);
    myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
    myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);    
 }

void turn_180(char direction){
  Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
  Adafruit_DCMotor *myMotor_l = AFMS.getMotor(1);
  Adafruit_DCMotor *myMotor_r = AFMS.getMotor(2);
  // create with the default frequency 1.6KHz
  AFMS.begin(); 

  
  // If direction is 'c' then run this code - turns clockwise
  // Steps are: 1) Turn 90 deg clockwise for 4 secs 2) Move forwards slowly for 5 seconds 3) Turn 90 deg clockwise for 4 secs 4) Stop
  if (direction=='c'){
  myMotor_l->setSpeed(150); myMotor_r->setSpeed(0);
  myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
  delay(4000);
  myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
  myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
  delay(4000);
  myMotor_l->setSpeed(150); myMotor_r->setSpeed(0);
  myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
  delay(4000);
  myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
  myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
  }

  // If direction is 'a' then run this code - turns anticlockwise
  // Steps are: 1) Turn 90 deg anitclockwise for 4 secs 2) Move forwards slowly for 5 seconds 3) Turn 90 deg anticlockwise for 4 secs 4) Stop
  else if (direction=='a') {    
  myMotor_l->setSpeed(0);myMotor_r->setSpeed(150);
  myMotor_l->run(BACKWARD);myMotor_r->run(FORWARD);
  delay(4000);
  myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
  myMotor_l->run(FORWARD);myMotor_r->run(FORWARD);
  delay(5000);
  myMotor_l->setSpeed(0);myMotor_r->setSpeed(150);
  myMotor_l->run(BACKWARD);myMotor_r->run(FORWARD);
  delay(4000);
  myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
  myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
  }
}

void move_forwards(int v)
{
  Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
  Adafruit_DCMotor *myMotor_l = AFMS.getMotor(1);
  Adafruit_DCMotor *myMotor_r = AFMS.getMotor(2);
  // create with the default frequency 1.6KHz
  AFMS.begin(); 

  // Run both motors
  myMotor_l->setSpeed(v); myMotor_r->setSpeed(v);
  myMotor_l->run(FORWARD);myMotor_r->run(FORWARD);
}

void lifting_motor(int v)
{
  Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
  Adafruit_DCMotor *myMotor_lm = AFMS.getMotor(4);
  // create with the default frequency 1.6KHz
  AFMS.begin(); 

  if (v > 0)
  {
    myMotor_lm->setSpeed(v);
    myMotor_lm->run(FORWARD);
  }

  else if (v < 0)
  {
    myMotor_lm->setSpeed(-v);
    myMotor_lm->run(BACKWARD);
  }
}

void setup() {
  // put your setup code here, to run once:
}
void loop() {
  Serial.println("ghwefjy");
  move_wheels(40,40);
  delay(5000);
move_wheels(0, 0);
  delay(10000);
  

  
  // put your main code here, to run repeatedly:

}
