// returns the distance measured by the ultrasound sensor.
float ultrasound(){
  // The Trigger pin will tell the sensor to range find
 // pinMode(TRIG_PIN, OUTPUT);
  //digitalWrite(TRIG_PIN, LOW);
  // We'll use the serial monitor to view the sensor output
  //Serial.begin(9600);
  unsigned long t1;
  unsigned long t2;
  unsigned long pulse_width;
  float result;
  float cm;
  float mm;

  // Hold the trigger pin high for at least 10 us
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  // Wait for pulse on echo pin
  while ( digitalRead(ECHO_PIN) == 0 );

  // Measure how long the echo pin was held high (pulse width)
  // Note: the micros() counter will overflow after ~70 min
  t1 = micros();
  while ( digitalRead(ECHO_PIN) == 1);
  t2 = micros();
  pulse_width = t2 - t1;

  // Calculate distance in centimeters The constants
  // are found in the datasheet, and calculated from the assumed speed 
  //of sound in air at sea level (~340 m/s).
  cm = pulse_width / 58.0;
  mm = cm * 10;

  // Print out results to serial monitor and return the distance in cm
  if ( pulse_width > MAX_DIST ) {
    Serial.println("Out of range");
  } else {
    Serial.print(cm);
    Serial.println(" cm \t");
    return cm;
  }
  
  
}
  
  
  
