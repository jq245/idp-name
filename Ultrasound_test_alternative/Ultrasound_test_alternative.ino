/**
 * HC-SR04 Demo
 * Demonstration of the HC-SR04 Ultrasonic Sensor
 * Date: August 3, 2016
 * 
 * Description:
 *  Connect the ultrasonic sensor to the Arduino as per the
 *  hardware connections below. Run the sketch and open a serial
 *  monitor. The distance read from the sensor will be displayed
 *  in centimeters and inches.
 * 
 * Hardware Connections:
 *  Arduino | HC-SR04 
 *  -------------------
 *    5V    |   VCC     
 *    13     |   Trig     
 *    12     |   Echo     
 *    GND   |   GND
 *  
 * License:
 *  Public Domain
 */
// Pins
const int TRIG_PIN = 5;
const int ECHO_PIN = 4;
//const int TRIG_PIN_2 = 11;
//const int ECHO_PIN_2 = 10;

// Anything over 400 cm (23200 us pulse) is "out of range"
const unsigned int MAX_DIST = 23200;

void setup() {

  // The Trigger pin will tell the sensor to range find
  pinMode(TRIG_PIN, OUTPUT);
  digitalWrite(TRIG_PIN, LOW);

  // We'll use the serial monitor to view the sensor output
  Serial.begin(9600);
}

void loop() {
// Call ultrasound function to get the distance in cm
ultrasound();
// Wait 100ms before next measurement
delay(100);
}
