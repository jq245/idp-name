// Code for NEW IR sensor
const int IR_PIN = 8;


void setup() {
  // put your setup code here, to run once:
  pinMode(IR_PIN, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (digitalRead(IR_PIN) == HIGH){
    Serial.println("Block not detected");
  }

  else if (digitalRead(IR_PIN) == LOW){
    Serial.println("Block detected");
  }

  delay(100);
}


// Code for OLD IR sensor
/*

// Set the global variables
int sensorPin = A0; // Using analog pin 0 (IR sensor output connected to A0)
int sensorValue = 0; // intialise variable to store value from sensor

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //Start serial and set the correct Baud Rate

}

void loop() {
  // put your main code here, to run repeatedly:
  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue);
  delay(100);
}
*/
