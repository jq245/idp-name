void rotate_servo3(char position, int home_pos, int angle)
{
  //Servo SortingServo;  // Create servo object
  //SortingServo.attach(7);   // Servo is connected to pin 7

  if (position == 'l')
  {
    SortingServo.write(home_pos + angle); // Rotate to 'angle' deg anticlockwise of home
    delay(500); // Wait for servo to get there
  }

  else if (position == 'r')
  {
    SortingServo.write(home_pos - angle); // Rotate to 'angle' deg clockwise of home
    delay(500); // Wait for servo to get there
  }

  else if (position == 'h')
  {
    SortingServo.write(home_pos); // Rotate to home position (94 deg)
    delay(500); // Wait for servo to get there
  }
}
