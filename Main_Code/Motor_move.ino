#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
void move_wheels(int v_l, int v_r){
   if (v_l == 0 && v_r == 0)
   {
    // Turn off flashing amber LED
    digitalWrite(MOTOR_RUNNING_PIN, LOW);
   }

   else
   {
    // Turn on flashing amber LED
    digitalWrite(MOTOR_RUNNING_PIN, HIGH);
   }
    // Setup motor
    Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
    Adafruit_DCMotor *myMotor_l = AFMS.getMotor(1);
    Adafruit_DCMotor *myMotor_r = AFMS.getMotor(2);
    // create with the default frequency 1.6KHz
    AFMS.begin(); 

  // Run either motor forwards or backwards depending on sign of speed argument
  if(v_l >=0 and v_r >=0){
    myMotor_l->setSpeed(v_l);myMotor_r->setSpeed(v_r);
    myMotor_l->run(FORWARD);myMotor_r->run(FORWARD);
    }

  else if(v_l <0 and v_r >=0){
    myMotor_l->setSpeed(-v_l); myMotor_r->setSpeed(v_r);
    myMotor_l->run(BACKWARD);myMotor_r->run(FORWARD);
    }
  else if(v_l >=0 and v_r <0){
    myMotor_l->setSpeed(v_l); myMotor_r->setSpeed(-v_r);
    myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
    }

  else if(v_l <0 and v_r <0){
    myMotor_l->setSpeed(-v_l); myMotor_r->setSpeed(-v_r);
    myMotor_l->run(BACKWARD);myMotor_r->run(BACKWARD);
    }
    
    
    
    
  }

void turn_90_right(){
    // Turn on flashing amber LED
    digitalWrite(MOTOR_RUNNING_PIN, HIGH);
    // Motor setup
    Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
    Adafruit_DCMotor *myMotor_l = AFMS.getMotor(1);
    Adafruit_DCMotor *myMotor_r = AFMS.getMotor(2);
    AFMS.begin(); 
    
    myMotor_l->setSpeed(120); myMotor_r->setSpeed(10);
    myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
    delay(4000);
    myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
    myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
    
    // Turn off flashing amber LED
    digitalWrite(MOTOR_RUNNING_PIN, LOW);
 }

void turn_90_left(){
    // Turn on flashing amber LED
    digitalWrite(MOTOR_RUNNING_PIN, HIGH);
    
    // Set up motors
    Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
    Adafruit_DCMotor *myMotor_l = AFMS.getMotor(1);
    Adafruit_DCMotor *myMotor_r = AFMS.getMotor(2);
    AFMS.begin(); 
    
    myMotor_l->setSpeed(10); myMotor_r->setSpeed(120);
    myMotor_l->run(FORWARD);myMotor_r->run(FORWARD);
    delay(5000);
    myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
    myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);   
     
    // Turn off flashing amber LED
    digitalWrite(MOTOR_RUNNING_PIN, LOW);
 }

void turn_180(char direction){
  // Turn on flashing amber LED
  digitalWrite(MOTOR_RUNNING_PIN, HIGH);
  // Set up motors
  Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
  Adafruit_DCMotor *myMotor_l = AFMS.getMotor(1);
  Adafruit_DCMotor *myMotor_r = AFMS.getMotor(2);
  AFMS.begin(); 

  // If direction is 'c' then run this code - turns clockwise
  if (direction=='c'){
  myMotor_l->setSpeed(150); myMotor_r->setSpeed(0);
  myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
  delay(3700);
  myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
  myMotor_l->run(BACKWARD);myMotor_r->run(BACKWARD);
  delay(500);
  myMotor_l->setSpeed(150); myMotor_r->setSpeed(0);
  myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);
  delay(3500);
  myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
  myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);

  // Turn off flashing amber LED
  digitalWrite(MOTOR_RUNNING_PIN, LOW);
  }

  // If direction is 'a' then run this code - turns anticlockwise
  else if (direction=='a') {    
  myMotor_l->setSpeed(0);myMotor_r->setSpeed(150);
  myMotor_l->run(BACKWARD);myMotor_r->run(FORWARD);
  delay(3700);
  myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
  myMotor_l->run(BACKWARD);myMotor_r->run(BACKWARD);
  delay(500);
  myMotor_l->setSpeed(0);myMotor_r->setSpeed(150);
  myMotor_l->run(BACKWARD);myMotor_r->run(FORWARD);
  delay(3500);
  myMotor_l->setSpeed(0); myMotor_r->setSpeed(0);
  myMotor_l->run(FORWARD);myMotor_r->run(BACKWARD);

  // Turn off flashing amber LED
  digitalWrite(MOTOR_RUNNING_PIN, LOW);
  }
}

// Function to run lifting motor
void lifting_motor(int v)
{
  // Setup lifting motor
  Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
  Adafruit_DCMotor *myMotor_lm = AFMS.getMotor(4);
  AFMS.begin(); 
  // If argument is positive, run motor forwards
  if (v > 0)
  {
    myMotor_lm->setSpeed(v);
    myMotor_lm->run(FORWARD);
  }
  // If argument is negative, run motor backwards
  else if (v < 0)
  {
    myMotor_lm->setSpeed(-v);
    myMotor_lm->run(BACKWARD);
  }
}
