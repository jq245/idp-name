// Function which returns the distance measured by the ultrasound sensor
float ultrasound(char position){
  // Variables
  unsigned long t1;
  unsigned long t2;
  unsigned long pulse_width;
  float result;
  float cm;
  float mm;

  // Getting front ultrasound sensor reading
  if(position == 'f')
  { 
  // Hold the trigger pin high for at least 10 us
  digitalWrite(TRIG_PIN_F, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_F, LOW);

  // Wait for pulse on echo pin
  while ( digitalRead(ECHO_PIN_F) == 0 );

  // Measure how long the echo pin was held high (pulse width)
  // Note: the micros() counter will overflow after ~70 min
  t1 = micros();
  while ( digitalRead(ECHO_PIN_F) == 1);
  t2 = micros();
  pulse_width = t2 - t1;
  cm = pulse_width / 58.0;
  
  if ( pulse_width > MAX_DIST ) {
    Serial.println("Out of range");
    // Sensor reading is usually out of range when too close to a wall (rather than too far)
    // Hence return 2cm if out of range
    cm = 2; 
    }
  Serial.print("                                               front sensor reading:");
  Serial.println(cm);
  // Return the distance measured in cm
  return cm;
  }

  // Getting side ultrasound reading
  else if(position == 's'){
  // Hold the trigger pin high for at least 10 us
  digitalWrite(TRIG_PIN_S, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_S, LOW);

  // Wait for pulse on echo pin
  while ( digitalRead(ECHO_PIN_S) == 0 );

  // Measure how long the echo pin was held high (pulse width)
  // Note: the micros() counter will overflow after ~70 min
  Serial.println(t1);
  t1 = micros();
  while ( digitalRead(ECHO_PIN_S) == 1);
  t2 = micros();
  pulse_width = t2 - t1;
  cm = pulse_width / 58.0;

  if ( pulse_width > MAX_DIST ) {
    Serial.println("Out of range");
    // Sensor reading is usually out of range when too close to a wall (rather than too far)
    // Hence return 2cm if out of range
    cm = 2;
  }
  
  Serial.print("               side sensor reading:");
  Serial.println(cm);
  return cm;
  }

  else
  {
    Serial.print("Invalid argument");
  }
  
  
}
