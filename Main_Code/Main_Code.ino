// Include the Arduino servo library
#include <Servo.h>
// Create servo object to control the sorting servo
Servo SortingServo;  

// Pins for ULTRASOUND
const int TRIG_PIN_F = 13;
const int ECHO_PIN_F = 12;
const int TRIG_PIN_S = 5;
const int ECHO_PIN_S = 4;
// Global variable for max distance ultrasound sensor can detect
const unsigned int MAX_DIST = 23200;// Anything over 400 cm (23200 us pulse) is "out of range"

// Pin for IR sensor
const int IR_PIN = 8;

// Pin for hall sensor;
const int HALL_SENSOR_PIN = 3;

// COUNTERS
int counter = 0;
int counter_90_right = 0; 
int counter_90_left = 0;

// REVERSING
int number_of_reverses = 0;

// PIN TO DETERMINE WHETHER MOTOR EITHER MOTOR IS RUNNING
const int MOTOR_RUNNING_PIN = 6;

// Global variable to store previous front ultrasound reading
float previous_ultrasound_front;


// SETUP
void setup() {
  // Use the serial monitor to view the sensor output
  Serial.begin(9600);
  
  // ULTRASOUND setup
  pinMode(TRIG_PIN_F, OUTPUT);
  digitalWrite(TRIG_PIN_F, LOW); 
  pinMode(TRIG_PIN_S, OUTPUT);
  digitalWrite(TRIG_PIN_S, LOW);

  // SERVO setup
  SortingServo.attach(10);  // attaches the servo on pin 10 to the servo object
  rotate_servo('h', 130, 30); // Make sure the servo is at the home position
  delay(1000);  // Delay a second to ensure servo has moved to home position
  
  // IR setup
  pinMode(IR_PIN, INPUT);
  
  // Hall sensor setup
  pinMode(HALL_SENSOR_PIN, INPUT);
  
  // Is motor running pin (for flashing amber LED) - set to LOW at start
  pinMode(MOTOR_RUNNING_PIN, OUTPUT);
  digitalWrite(MOTOR_RUNNING_PIN, LOW);
}

void loop() {

  // Create variable for the left and right wheel speed
  float v_l = 150;
  float v_r = 150;
  
  // Check if a fuel cell has been detected by IR sensor
  if (is_block() == true && counter < 7)
  {
    // If fuel cell is detected and counter is less than 7 (robot still in red area)
    fuel_cell(); // Run code for when fuel cell is detected
    
  }
  

  
  // First part: moving forwards until reaches first wall and then make a 90 degree turn
  if(counter == 0)
  {
    Serial.println("Counter 0");
    // Move forwards with speed 200
    move_wheels(200,200);
    counter = 1;
  }

  // Use PD control to ensure steer robot and then move in a straight line at a distance of 34cm from left wall, until robot reaches wall in front of it
  if (counter ==1 && ultrasound('f') > 10  && counter_90_right ==0 && previous_ultrasound_front >10)
  {
    Serial.println("First part: moving forwards until reaches first wall, counter = 1 =========================");
    Serial.println("Start fine tuning");
    fine_tuning(34, v_l, v_r);
    move_wheels(v_l,v_r);
  }
  
  // Hit the wall to align, then move backwards, then turn 90 degrees clockwise when it reaches 30cm from first wall
  if(counter == 1 && counter_90_right == 0 && ultrasound('f') <= 10 && previous_ultrasound_front <=10 )
  {
    // Hit wall to align
    Serial.println("First part: aligning, counter = 1, counter 90 = 0 ---------------------------------------");
    move_wheels(150,150);
    delay(1000);
    counter_90_right = 1;
  }
    
  if (counter== 1 && counter_90_right ==1 && ultrasound('f') < 30 && previous_ultrasound_front < 30){
    // Move backwards until 30cm from wall
    move_wheels(-150, -150);
  }
  if (counter== 1 && counter_90_right ==1 && ultrasound('f') > 30 && previous_ultrasound_front > 30){
    // Turn right
    Serial.println("First part: turning right, counter = 1, counter 90 = 1-----------------------------------");
    move_wheels(145, 30);
    delay(5500);
    // Reverse into wall to align
    move_wheels(-175, -175);
    delay(3500);
    counter = 2;
}
  
 // Collecting the first 5 fuel cells 
  if (counter ==2 && ultrasound('f') > 8 && previous_ultrasound_front > 8)
  {
    Serial.println("First part: follow the path that leads to the fixed cells--------------------------------");
    // Move in straight line 9 cm from left wall to pick up 5 fuel cells
    fine_tuning_2(9, v_l, v_r);
    move_wheels(v_l,v_r);
  }

 
  // Second part:
  // When robot reaches second wall, turn 180 degrees clockwise (90 clockwise, forwards, 90 clockwise), then reverse into wall to align itself
  if (counter == 2 && ultrasound('f') < 8 && previous_ultrasound_front < 8)
  { 
  Serial.println("Second part: starting turning 180 for the first time, counter = 2 =============================");
  // Hit the wall
  move_wheels(150,150);
  delay(1000);
  // Reverse a bit
  move_wheels(-200, -200);
  delay(4000);
  // Turn 90 right
  move_wheels(220, 100);
  delay(4500);
  // Reverse to align
  move_wheels(-170, -170);
  delay(1000);
  // Turn 90 right
  move_wheels(105, 0);
  delay(5000);
  // Reverse to align
  move_wheels(-175, -175);
  delay(3000); 
  counter = 3;
  }
 
  // Third  part:
  // Moving in straight line after it has turned 180
  if (counter == 3 && ultrasound('f') > 100 && previous_ultrasound_front > 100)
  {
    // Move forwards
    Serial.println("Third part: Moving straight after 180, no fine tuning ========================================");
    move_wheels(200, 200);   
  }
  if (counter == 3 && ultrasound('f') <= 100 && ultrasound('f') > 10 && previous_ultrasound_front > 10)
  {
    // Start using PD control when get to 100cm from wall (unreliable ultrasound readings so PD control not used immediately) 
    Serial.println("Third part: with fine tuning");
    fine_tuning_2(175, v_l, v_r);
    move_wheels(v_l,v_r);
  }

  
  // Forth part:
  // When hit wall, move forwards to ensure robot aligned, then reverse, then turn 180 for the second time
  if (counter ==3 && ultrasound('f') <= 10 && previous_ultrasound_front <= 10)
  {
    // Go forwards to hit wall and align itself
    Serial.println("Forth part: Forwards to hit wall and align, counter = 3, second 180 ==========================");
    move_wheels(150,150);
    delay(1500);
    // Reverse a bit
    move_wheels(-200,-200);
    delay(3000);
    // Turn 180 degrees
    Serial.println("Turning 180 degrees anticlockwise");
    turn_180('a');
    // Reverse to align
    Serial.println("Reverse to align");
    move_wheels(-175, -175);
    delay(5000);
    counter =4;
  }


  // Fifth part: Moving in straight line after it has turned 180
  if (counter == 4 && ultrasound('f') > 8 && previous_ultrasound_front > 8)
  {
    Serial.println("Fifth part: third sweep, counter =4 =========================================================");
    // Use PD control to move in straight line 55cm from side wall
    fine_tuning_2(55, v_l, v_r);
    move_wheels(v_l,v_r);
  }


  // Sixth part:
  // When hit wall, move forwards to ensure robot aligned, then reverse, then turn 180 clockwise
  if (counter ==4 && ultrasound('f') < 8 && previous_ultrasound_front < 8)
  {
    // Go forwards to hit wall and align itself
    Serial.println("Sixth part: align, third 180, counter =4======================================================");
    move_wheels(150,150);
    delay(1500);
    // Reverse a bit
    move_wheels(-200,-200);
    delay(3000);
    // Turn 180 degrees clockwise
    Serial.println("Turning 180 degrees clockwise");
    turn_180('c');
    // Reverse to align
    Serial.println("Reverse to align");
    move_wheels(-175, -175);
    delay(4500);
    counter =5;
  }
  
  
// Seventh part: moving forward 
    if (counter == 5 && ultrasound('f') > 8 && previous_ultrasound_front > 8)
  {
    Serial.println("Seventh part: forth sweeping, counter =5======================================================");
    // Use PD control to move in straight line 120cm from side wall
    fine_tuning_2(120, v_l, v_r);
    move_wheels(v_l,v_r);
  }

  if (counter == 5 && ultrasound('f') < 8 && previous_ultrasound_front < 8){
  
  Serial.println("Aligning and turning 90 to the left, counter 7======================================================="); 
  // Hit wall to align
  move_wheels(150, 150);
  delay(1000);
  // Reverse a bit
  move_wheels(-200, -200);
  delay(4000);
  // Turn left to exit the red zone and enter the safe area
  turn_90_left();
  counter = 6;
  }

 if (counter ==6 && ultrasound('f') > 10 && counter_90_left == 0 && previous_ultrasound_front > 10)
  {
    // Move forwards
    move_wheels(200,200);
  }
  
  // Hit the wall to align
  if(counter == 6 && counter_90_left == 0 && ultrasound('f') <= 10 && previous_ultrasound_front <= 10)
  {
  Serial.println(" Aligning---------------------------------------");
  move_wheels(150,150);
  delay(2500);
  counter_90_left = 1;
  }

 // Reverse until 40cm from wall
 if (counter== 6 && counter_90_left ==1 && ultrasound('f') < 40 ){
    move_wheels(-150, -150);
   }
 // Turn left  
   if (counter== 6 && counter_90_left ==1 && ultrasound('f') > 40 ){
    Serial.println("turn 90 left again when front sensor > 40 , counter 6-----------------------------------");
    move_wheels(30, 140);
    delay(5800);
    counter = 7;
  }

  
  // Move forwards and hit the wall - ideally fine_tuning (PD control) would be used to align the robot with the shelf directly 
  // but not possible due to highly inaccurate front ultrasound sensor readings far away from wall
  // So move forwards with both wheels set to same speed and hit wall to align instead, then reverse away from wall (to get accurate front ultrasound readings)
  if (counter == 7 && ultrasound('f') > 8 && previous_ultrasound_front > 8)
  {
    // Move forwards until reach wall
    Serial.println("Move forwards");
    move_wheels(200, 200);
  }

  if (counter == 7 && ultrasound('f') < 8 && previous_ultrasound_front < 8 )
  {
    Serial.println("Hit wall to align");
    move_wheels(150,150);
    delay(1000);
    counter = 8;
    
  }
  
  // Once robot reaches wall, measure distance from side of robot to side wall (reasonably reliable)
  // Perform a number of reverse turns until the robot is aligned with the shelf
  if (counter == 8)
  {
    int k = 100;
    int sum = 0;
    int n = 0;
    Serial.println("starting the loop");
    Serial.println(k);
    // Measure side distance k (100) times and if distance is > 150 (to avoid anomalous distances being used, use it to calculate a mean side distance
    for (int a=0; a < k; a = a+1)
    {
      float reading = ultrasound('s');
      Serial.println(reading);
      if (reading > 150)
      {
        sum = sum + reading;
        n = n + 1;
      }
      delay(3000/k);
    }
    Serial.println(sum);    
    float mean_side_dist = sum / n;
    // Calculate distance between robot and shelf
    float difference = 204 - mean_side_dist;
    // Each reverse turn moves robot 3.6cm so number of reverse turns required can be calculated
    number_of_reverses = difference / 3.6;
    Serial.println("number_of_reverses");  
    Serial.println(number_of_reverses);
    Serial.println(difference);
    counter = 9;
    
  }
  // Perform the reverse turns
  if (counter == 9)
  {
    for (int a = number_of_reverses; a > 0; a = a -1)
    {
    Serial.println("Start parallel parking");
    move_wheels(0,0);
    delay(2000);
    move_wheels(-120, -80);
    delay(2000);
    move_wheels(-80, -120);
    delay(2000);
    move_wheels(255, 255);
    delay(1500);
    }
    counter = 10;
  }

  
  // Reversing almost straight (not exactly straight to avoid hitting shelf) (parallel park)
  if (counter == 10 && ultrasound('f') < 90 ){
    move_wheels(-150, -153);
    }

  // Stop robot once it reaches the shelf
  if (counter == 10 && ultrasound('f') > 90)
  {
   move_wheels(0,0); 
   Serial.println("Reached shelf");
   // Activate lifting motor to move platform up
   lifting_motor(230);
   delay(25000); // Lift for 25 seconds
   lifting_motor(0); // Stop lifting motor
   counter = 11;
  }

  // Go back to starting area
  if (counter == 11){
    move_wheels(-100, -150);
    delay(3000);
    move_wheels(-150, -100);
    delay(3500);
    move_wheels(-150, -150);
    delay(5000);
    move_wheels(0,0);
    delay(100000);
  }

  // Update the previous_ultrasound_front variable so we can double check all front ultrasound readings are sensible before acting upon them
  previous_ultrasound_front = ultrasound('f');
  Serial.print(previous_ultrasound_front);
  Serial.println("**************");

}
