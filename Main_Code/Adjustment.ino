// Given the measurement from the side ultrasound sensor and the reference value, the fine_tuning function returns the speeds of the two motors using proportional derivative control
float fine_tuning(float Rs, float& v_l, float& v_r)  // Rs is the side reference distance, v_l is the left wheel speed, v_r is the right wheel speed
{
  int kp = 3.5;
  int kd = 7;
  float error;
  delay(10);
  
  // Measure side ultrasound sensor distance
  float side_distance_t = ultrasound('s');
  // Delay 500ms
  delay(500);
  // Measure side ultrasound sensor distance again
  float side_distance_t_dt = ultrasound('s');
  // Calculate the instantaneous component of velocity (towards/away from wall) using the two distances measured
  float vel = (side_distance_t_dt - side_distance_t) / 0.5;
  // Calculate the difference in the required side distance and the actual measured side distance
  float difference = side_distance_t_dt - Rs;
  // Calculate the error
  error = kp*(difference) + kd*vel;

  // Limit the error to +/- 15 so inaccurate sensor readings do not cause the robot is veer too much
  if (error >= 15){
    error = 15;
    }
  if (error <= -15){
    error = -15;
    }
  // Set the left and right motor speeds
  v_l = 200 - error;
  v_r = 200 + error;
  // Print readings and calculated values to serial
  Serial.print("velocity:");
  Serial.println(vel);
  Serial.print("difference:");
  Serial.println(difference);
  Serial.print("error:");
  Serial.println(error);
  Serial.print("                              Left wheel speed:");
  Serial.println(v_l);
  Serial.print("                              Right wheel speed:");
  Serial.println(v_r);

}

// A slightly different fine tuning function with different values for kp, kd and limits error to +/- 2 for slight adjustment only
float fine_tuning_2(float Rs, float& v_l, float& v_r)  // Rs is the side reference distance
{
  int kp = 10;
  int kd = 5;
  float error;
  float side_distance_t = ultrasound('s');
  delay(500);
  float side_distance_t_dt = ultrasound('s');
  float vel = (side_distance_t_dt - side_distance_t) / 0.5;
  float difference = side_distance_t_dt - Rs;
  error = kp*(difference) + kd*vel;
  if (error >= 10){
    error = 2;
    }
  if (error <= -10){
    error = -2;
    }
  v_l = 200 - error;
  v_r = 200 + error;
    Serial.print("velocity:");
    Serial.println(vel);
    Serial.print("difference:");
    Serial.println(difference);
    Serial.print("error:");
    Serial.println(error);
    Serial.print("                              Left wheel speed:");
    Serial.println(v_l);
    Serial.print("                              Right wheel speed:");
    Serial.println(v_r);

}
