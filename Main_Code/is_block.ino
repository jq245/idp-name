// Function which returns true if fuel cell is detected and false otherwise
bool is_block(){
  // Take digital reading from IR sensor - if low, then fuel cell is detected
  if (digitalRead(IR_PIN) == LOW)
  {
    return true;
  }
  
  else
  {
    return false;
  }
}
