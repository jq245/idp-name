void fuel_cell()
{
    move_wheels(0,0); // Stop the robot when block is detected
    Serial.println("FUEL CELL DETECTED");
    delay(1000); // Wait 1 second
    move_wheels(40,40); // Move forwards for 1 second
    delay(1000);
    move_wheels(0,0);
    bool active = false;
    int n = 300; // Check to see if fuel cell is active n times

    for ( int a = 0; a < n; a = a + 1)
    {
       if (digitalRead(HALL_SENSOR_PIN) == HIGH)
       {
        // Fuel cell is deemed active if at least one of the readings from the hall sensor is high
        active = true;
        Serial.println("Active");
       }

       else {
        Serial.println("Inactive");
       }

       delay(2000/n); // Check for 2 seconds (to get around intermittent hall sensor reading)
    }
    
    
    if (active) // If active fuel cell detected
    {
      Serial.println("Fuel cell is ACTIVE");
      rotate_servo('r', 125, 20); // Rotate servo right a bit
      move_wheels(40,40); // Robot moves forward for 5 seconds slowly to reject the fuel cell
      delay(5000);
      move_wheels(0,0);
      rotate_servo('h', 125, 30); // Servo goes back to home position
      delay(1000);
      Serial.println("Return to path");
    }

    else  // If inactive fuel cell detected
    {
          Serial.println("Fuel cell is INACTIVE");
          rotate_servo('l', 125, 25); // Rotate servo left a bit
          move_wheels(20,20); // Move forwards for 2 seconds slowly so fuel cell is against sweeping arm
          delay(5500);
          move_wheels(0,0);
          rotate_servo('r', 125, 90); // Rotate servo to swing fuel cell onto platform
          delay(2000);
          rotate_servo('h', 125, 30);// Servo goes back to home position
          delay(2000);
          rotate_servo('r', 125, 90); // Rotate servo to swing fuel cell onto platform
          delay(2000);
          rotate_servo('h', 125, 30); // Servo goes back to home position
          delay(2000);
          Serial.println("Return to path");
    }
}
