// Function which rotates the servo (for swinging lever arm)
void rotate_servo(char position, int home_pos, int angle)
{
  if (position == 'l')
  {
    SortingServo.write(home_pos + angle); // Rotate to 'angle' deg anticlockwise of home
    delay(500); // Wait for servo to get there
  }

  else if (position == 'r')
  {
    SortingServo.write(home_pos - angle); // Rotate to 'angle' deg clockwise of home
    delay(500); // Wait for servo to get there
  }

  else if (position == 'h')
  {
    SortingServo.write(home_pos); // Rotate to home position
    delay(500); // Wait for servo to get there
  }
}
